# Ansible Role: Docker

An Ansible Role that installs [Docker](https://www.docker.com) on Linux.

## Requirements
None.

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    # Edition can be one of: 'ce' (Community Edition) or 'ee' (Enterprise Edition).
    docker_edition: 'ce'
    docker_packages:
        - "docker-{{ docker_edition }}"
        - "docker-{{ docker_edition }}-cli"
    docker_packages_state: present

The `docker_edition` should be either `ce` (Community Edition) or `ee` (Enterprise Edition). 
You can also specify a specific version of Docker to install using the distribution-specific format: 
`docker-{{ docker_edition }}=<VERSION>`.

You can control whether the package is installed, uninstalled, or at the latest version by setting `docker_package_state` to `present`, `absent`, or `latest`, respectively. Note that the Docker daemon will be automatically restarted if the Docker package is updated. This is a side effect of flushing all handlers (running any of the handlers that have been notified by this and any other role up to this point in the play).

    docker_service_manage: true
    docker_service_state: started
    docker_service_enabled: true
    docker_restart_handler_state: restarted

Variables to control the state of the `docker` service, and whether it should start on boot. If you're installing Docker inside a Docker container without systemd or sysvinit, you should set `docker_service_manage` to `false`.

    docker_compose_install_plugin: false
    docker_compose_package: docker-compose-plugin
    docker_compose_package_state: present

Docker Compose Plugin installation options. These differ from the below in that docker-compose is installed as a docker plugin (and used with `docker compose`) instead of a standalone binary.

    docker_compose_install: true
    docker_compose_version: "1.26.0"
    docker_compose_arch: "x86_64"
    docker_compose_path: /usr/local/bin/docker-compose

Docker Compose installation options.

    docker_add_repo: true

Controls whether this role will add the official Docker repository. Set to `false` if you want to use the default docker packages for your system or manage the package repository on your own.

    docker_repo_url: https://download.docker.com/linux

The main Docker repo URL, common between Debian systems.

    docker_apt_release_channel: stable
    docker_apt_arch: "amd64"
    docker_apt_repository: "deb [arch={{ docker_apt_arch }}] {{ docker_repo_url }}/ubuntu focal {{ docker_apt_release_channel }}"
    docker_apt_ignore_key_error: true
    docker_apt_gpg_key: "{{ docker_repo_url }}/ubuntu/gpg"

You can switch the channel to `nightly` if you want to use the Nightly release.

    docker_users:
      - user1
      - user2

A list of system users to be added to the `docker` group (so they can use Docker on the server).

    docker_daemon_options:
      storage-driver: "devicemapper"
      log-opts:
        max-size: "100m"

Custom `dockerd` options can be configured through this dictionary representing the json file `/etc/docker/daemon.json`.


## Dependencies

None.

## Example Playbook

```yaml
- hosts: all
  roles:
    - docker
```

## License

MIT / BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
